# StartPage Project #

This is a side project to create an awesome looking & highly productive/useful start page that can open as the default page on the new tab.

### Road Map ###

* List some ideas for a start page to make me productive
* find problems, find solutions
* Release v0.0.1
---

### List of really common features among available start pages ###
* Thumbnails for access to frequently visited websites
* See bookmarks & recently closed tabs
* custom background image
* configure shortcuts
* widgets -
    * notes
    * clock
    * weather
    * to-do list
* (collapsible) sidebar to access bookmars / recently closed tabs
* random article from wikipedia
* emails (short set of recent emails)
* rss feed
* adjectives: responsive, simple, easily editable, clean, fast

### smart things ###
* 'begin' link to google
* 'takethisalong' popup on clicking on a link
* (The F\*\*king Weather)[http://thefuckingweather.com]
* references
    * http://folter-x.deviantart.com/art/start-page-138905304
    * http://noirtek.deviantart.com/art/noirtek-startpage-155756669
    * http://folter-x.deviantart.com/art/start-page-160869457
    * http://mmesantos1.deviantart.com/art/Goodbye-2011-273885488
    * http://mmesantos1.deviantart.com/art/Uniform-Icon-Theme-453054609
    * http://lastochki.deviantart.com/art/le-ciel-180385989

### List of Ideas To Implement in v0.0.1 ###
* Categorized Jump Links or Lists
    * News
    * Social
    * Design
    * Web Development
    * Automobile
    * Tools
* To Do
* Notes App - short notes to long blog posts, write everything!
* Calendar of events
---

### How to contribute ###
I am finalizing the list of features shipping in version 0.0.1. Contributions will start at a later point.